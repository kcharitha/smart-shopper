import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { DataShareService } from "../services/data-share.service";
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  dropdownList: any = [];
  selectedItems: any[];
  dropdownSettings: any;
  location: string = "";
  constructor(private dataServ: DataShareService) { }

  ngOnInit() {
   // localStorage.removeItem("banks")
    this.dropdownSettings = {
      singleSelection: false,
      text: "Select Bank",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class pull-right selectedBox "
    };

    this.dropdownList = [
      { "id": 1, "itemName": "US Bank" },
      { "id": 2, "itemName": "Federal Bank" },
      { "id": 3, "itemName": "Citi Bank" },
      { "id": 4, "itemName": "Master Bank" }

    ];
    this.selectedItems = this.getSavedBankDetails();

    this.dataServ.locName.subscribe((data) => {
      this.location = data;
    });
  }


  changeSelectBank(items: any) {
    this.dataServ.setBankDetails(this.selectedItems);
    console.log(items)
    localStorage.setItem('banks', JSON.stringify(this.selectedItems));
  }


  getSavedBankDetails() {
    let sel = JSON.parse(localStorage.getItem('banks'));
    console.log(localStorage.getItem('banks'))
    if (!sel) {
      sel = [
        { "id": 1, "itemName": "US Bank" },
        { "id": 2, "itemName": "Federal Bank" }
      ];
      localStorage.setItem('banks', JSON.stringify(sel));
    }
    return sel;
  }

}
