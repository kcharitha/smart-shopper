
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import { DataShareService } from "../services/data-share.service";
@Component({
  selector: 'app-show-details',
  templateUrl: './show-details.component.html',
  styleUrls: ['./show-details.component.css']
})
export class ShowDetailsComponent implements OnInit {
  offersList: any;
  currentLoc: any;
  currentLocFlag: EventEmitter<any> = new EventEmitter<any>();
  sub: any;
  businessData: any;
  businessDataFlag: EventEmitter<any> = new EventEmitter<any>();
  offers: any;
  offersFlag: EventEmitter<any> = new EventEmitter<any>();
  locName:string="";


  constructor(private http: HttpClient, private dataServ: DataShareService) { }

  getMyLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((pos) => {
        this.currentLoc = {
          "latitude": pos.coords.latitude.toFixed(2),
          "longitude": pos.coords.longitude.toFixed(2)
        }

        //for testing other options
/*
        this.currentLoc = {
          "latitude": 10,
          "longitude":20
        }
        */
        this.currentLocFlag.emit(true);
      });
    }
  }


  getOfferDetails(selectedBank) {
    let getOffersURL;

    if (selectedBank.length == 1) {
      getOffersURL = "src/data/offerDetails1Bank.json";
    } else {
      getOffersURL = "src/data/offerDetails.json";
    }

    this.getData(getOffersURL).subscribe((data: any) => {
    //  console.log("5");
      this.offersList = data;
      this.offersFlag.emit(true);
    });
  }

  ngOnInit() {
    this.getMyLocation();

    this.dataServ.bankDetails.subscribe((data) => {
      this.getOfferDetails(data);

    })
    this.businessDataFlag.subscribe((data) => {
     // console.log(this.businessData)
     // console.log("4");
      this.getOfferDetails(JSON.parse(localStorage.getItem("banks")));
    });

    this.currentLocFlag.subscribe((data: any) => {
     // console.log(this.currentLoc)
     // console.log("2");
      let getShopNameURL = "src/data/businessData.json";
      this.getData(getShopNameURL).subscribe((data: any) => {
      //  console.log("3");
        this.businessData = data;
        this.businessDataFlag.emit(true);
      });
    }
    );


    this.offersFlag.subscribe((data) => {
    //  console.log(this.offersList)
    //  console.log("6");
      this.getOffers();
    });

  }

  getOffers() {
    let name = "";

    for (let i = 0; i < this.businessData.length; i++) {
      if (this.businessData[i].latitude == this.currentLoc.latitude && this.businessData[i].longitude == this.currentLoc.longitude) {
        name = this.businessData[i].name;
        break;
      }
    }
this.dataServ.setLocationName(name);
    this.offers = this.offersList[name];
    this.locName=name;
  //  console.log(this.offers)
  }

  getData(url) {
    return this.http.get(url);
  }


}
