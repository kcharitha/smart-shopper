import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { HttpClient } from "@angular/common/http";



import { AppComponent } from './app.component';
import { ShowDetailsComponent } from './show-details/show-details.component';
import { HeaderComponent } from './header/header.component';
import { OfferDataComponent } from './offer-data/offer-data.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import{DataShareService} from './services/data-share.service';

@NgModule({
  declarations: [
    AppComponent,
    ShowDetailsComponent,
    HeaderComponent,
    OfferDataComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    AngularMultiSelectModule,
    BrowserAnimationsModule
  ],
  providers: [HttpClient,DataShareService],
  bootstrap: [AppComponent]
})
export class AppModule { }
