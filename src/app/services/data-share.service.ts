import { Injectable, EventEmitter, Output } from '@angular/core';

@Injectable()
export class DataShareService {

    @Output() bankDetails: EventEmitter<any> = new EventEmitter<any>();
    @Output() locName: EventEmitter<any> = new EventEmitter<any>();
    constructor() { }

    setBankDetails(bankDetails) {
            this.bankDetails.emit(bankDetails);
    }

    setLocationName(name){
        this.locName.emit(name);

    }
}