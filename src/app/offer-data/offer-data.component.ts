import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-offer-data',
  templateUrl: './offer-data.component.html',
  styleUrls: ['./offer-data.component.css']
})
export class OfferDataComponent implements OnInit {


  @Input() offerData;
  moreData: any = [];
  constructor() { }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.offerData) {
      this.offerData = changes.offerData.currentValue;
      //  console.log(this.offerData)
      if (this.offerData) {
        
        this.moreData = []
        for (let i = 0; i < this.offerData.length; i++) {
          this.offerData[i]["src"] = "../../assets/images/" + this.offerData[i].type + ".jpg";

          console.log(i)

          let least = Number.POSITIVE_INFINITY;
          let more = Number.NEGATIVE_INFINITY;
          let tmp;

          for (let j = this.offerData[i].offers.length - 1; j >= 0; j--) {
            tmp = this.offerData[i].offers[j].per;
            if (tmp < least) least = tmp;

            if (tmp > more) {
              more = tmp;
              this.moreData[i] = {
                type: this.offerData[i].type,
                name: this.offerData[i].offers[j].name,
                val: more
              };

            }


          }



        };

        console.log(this.moreData);

      }


      //  console.log(this.offerData)
    }

  }

}
