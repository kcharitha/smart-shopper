# Project Name: Smart Shopper
This project is developed for KCFed Code-A-Thon event under the theme ‘Don’t Make Me Think!’. 

## Team Name: Squad4 – University of Nebraska at Omaha
### Team Members 
1.	Sushma Sakala
2.	Vamshi Krishna
3.	Lakshmi Prasanna Kaspa
4.	Charitha Karnam

## Project Goal: 
The project, smart shopper aims to provide enhanced frictionless shopping and financial suggestion to consumers. Project enables consumers to compare the different bank deals and choose the appropriate bank card to shop. This application brings seamless experience to clients by letting them focus on the decisions of shopping rather than having to spend time to think about which bank card to use.

## Project Description:
Consumers might have one or more than one bank card.Most banks offer cash back deals and discounts when customers use their credit or debit cards. Stores might offer coupon codes for discount. Customers have to manually compare all bank offers and store coupons to check which offer gives him most beneficial offer. This is a tedious task. Our project automatically tracks which store the consumer is in and shows a comparison of bank deals and store coupons, highlights the best deal offered for the store - all on their device. 

+ 	The Consumer on first login, saves his card details including the bank name and type of card.
+ 	The Consumer can give permission to automatically track his location when he is shopping.

## Project Features: 
To give consumer frictionless experience we are including following features

+	The Consumer can access the application on the go – the application automatically tracks his location, he need not enter any of his location details.
+	The Application then shows the deals that the banks offer for different categories including but not limited to Electronics, Apparels, Furniture, Automobiles in that particular store.
+	The Application calculates and highlights which bank offers the best deals, without letting the customer think.
+   The Consumer can directly use highlighted option to get best available offer.
+   The Application will be running in background so when ever change in location or bank details occurs it will automatically updates itself and provides best offer details.

## Future Work:
For time being we developed web application. We would like to add this application as a mobile app so that user need to just download the app and give bank details while downloading app. 		App will be running in the background and whenever user is in a shopping store he will automatically receive notification from app without even accessing the app.		Also, we plan to add additional feature which displays the graphical chart with the amount of money saved using one particular bank deal over the others.

## Programming Languages Used:
+	Angular JS
+	CSS3
+	HTML5

## Instructions to run the Project:
### Pre-requisites
+	Node.js
+	Angular CLI

### Steps to run
+ Run npm install to install all node modules.
+ Run ng serve --port for a dev server. 
+ Navigate to http://localhost:4200/ to view the web application.

## Youtube Link 
https://www.youtube.com/watch?v=WGTm6RkBUjo
